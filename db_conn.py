import os

import mysql.connector


def connect_db():
    return mysql.connector.connect(
        user=os.environ["DB_USER"],
        password=os.environ["DB_PASS"],
        database=os.environ["DB_DB"],
        host=os.environ["DB_HOST"]
    )

# sudo:
# creer utilisateur sans home sans mot de passe.
# 600 - tout le tree chown
# creer utilisateur de bdd avec les doits minimum
# user
#
