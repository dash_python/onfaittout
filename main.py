from typing import List

from fastapi import FastAPI, HTTPException

import crud
import schema
from db_conn import connect_db

app = FastAPI()
conn = connect_db()


# personne avec son pays d'origine et son pays de destination
@app.get("/person/travel_to")
def get_list_personne_destination() -> List[schema.PersonneDestinationOut]:
    """
    route qui retourne la liste des personne avec la liste de leur destination
    """
    cursor = conn.cursor(dictionary=True)
    request_result = crud.get_all_personne_destination(cursor)
    cursor.close()
    if not request_result:
        raise HTTPException(status_code=404, detail="Nothing found")
    # PAS CONTENT ! PAS CONTENT !
    response = []
    for result in request_result:
        if personne := next((r for r in response if r["id_personne"] == result["id_personne"]), None):
            personne["voyages"].append({"pays": result["pays"], "nb_jour_visite": result["duree"]})
            continue
        voyages = [{"pays": result.pop("pays"), "nb_jour_visite": result.pop("duree")}]
        personne = result
        personne["voyages"] = voyages
        response.append(personne)

    return [schema.PersonneDestinationOut(**data) for data in response]


@app.get("/voyages")
def get_all_voyages_data() -> List[schema.Voyage]:
    request_result = crud.get_all_personne_destination(conn.cursor(dictionary=True))
    if not request_result:
        raise HTTPException(status_code=404, detail="rien a voir ici pas de voyages enregistre")
    return [schema.Voyage(nb_jour_visite=data.pop('duree'), **data) for data in request_result]


@app.get("/country/days_visit")
def get_country_days_visit() -> List[schema.PaysNbJourVisiteOut]:
    cursor = conn.cursor(dictionary=True)
    request_result = crud.get_nb_visite_par_pays(cursor)
    if not request_result:
        raise HTTPException(status_code=404, detail="pas de donnees")
    return [schema.PaysNbJourVisiteOut(**data) for data in request_result]
