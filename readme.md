# OnFaitTout

## Installation/run

Si vous voulez tester avec des donnees random, il y a une mini base de donnees de test:

la database s'appele "onfaittout"

```shell
sudo mysql < scripts/test.sql
# n'oublier pas des donner les privileges a un utilisateur.
# on est sympa pas vrai ?
```

to install the api run this
```bash
git clone https://gitlab.com/onfaittout/onfaittout.git
cd onfaittout
chmod +x install.sh
./install.sh
```

to run the api run this
```shell
chmod +x run.sh
./run.sh
```

## Schéma des données retournées par l'api

### Pays avec le nombre de jours de visite

routes : ```/country/days_visit```

```json
{
    "pays":"string",
    "nb_jour_visite": int
}
```
### Une route qui permet de recuperer toutes les personnes avec leurs voyages

routes : ```/person/travel_to```

```json
[
  {
    "nom": "string",
    "prenom": "string",
    "pays_residence": "string",
    "voyages": [
      {
        "pays": "string",
        "nb_jour_visite": 0
      }
    ]
  }
]
```
### Une route qui permet de récupérer toutes les données avec une limite et un décalage (aka. limit, offset)

routes : ```/voyages```

json :
```json
[
  {
    "pays": "string",
    "nb_jour_visite": 0,
    "nom": "string",
    "prenom": "string",
    "pays_residence": "string"
  }
]
```


### Une route qui envoie des stats (pays le plus visité, moyenne de la durée des visites par pays, personne ayant le plus voyagé etc.)

c'est ```/voyages```
