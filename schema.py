from typing import List

from pydantic import BaseModel


class PaysBaseOut(BaseModel):
    pays: str


class PaysNbJourVisiteOut(PaysBaseOut):
    nb_jour_visite: int


class PersonneBaseOut(BaseModel):
    nom: str
    prenom: str
    pays_residence: str


class PersonneDestinationOut(PersonneBaseOut):
    voyages: List[PaysNbJourVisiteOut]


class Voyage(PersonneBaseOut, PaysNbJourVisiteOut):
    pass
