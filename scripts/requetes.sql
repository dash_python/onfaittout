# Route qui permet de récupérer les données en fonction du pays
SELECT pays.pays, SUM(voyage.duree) AS nombre_de_jours_visites
FROM pays
JOIN voyage ON pays.id_pays = voyage.id_pays
GROUP BY voyage.id_pays;

# Une route qui permet de récupérer les données en fonction de la personne
SELECT 
	p.id_personne, 
	p.nom, 
	p.prenom, 
	pa2.pays AS pays_residence, 
	pa.pays, 
	v.duree 
FROM voyage AS v 
JOIN personnes AS p ON p.id_personne = v.id_personne 
JOIN pays AS pa ON pa.id_pays = v.id_pays 
JOIN pays AS pa2 ON p.pays_residence = pa2.id_pays;

# Une route qui envoie des stats (pays le plus visité, moyenne de la durée des visites par pays, personne ayant le plus voyagé etc.)
#Pays le plus visité
SELECT 
	pays.pays, 
	COUNT(v.id_pays) AS nombre_de_visites 
FROM pays 
JOIN voyage as v on v.id_pays = pays.id_pays 
GROUP BY pays  
ORDER BY nombre_de_visites 
DESC LIMIT 1;

# Moyenne de la durée des visites par pays, ajouter durée avec dates
SELECT pays, AVG(voyage.duree) 
FROM pays
JOIN voyage ON pays.id_pays = voyage.id_pays
GROUP BY pays;

# Personne ayant le plus voyagé
SELECT prenom, nom, COUNT(voyage.id_pays) AS Nombre_de_pays FROM personnes
JOIN voyage ON personnes.id_personne = voyage.id_personne
JOIN pays ON voyage.id_pays = pays.id_pays
GROUP BY personnes.id_personne
ORDER BY Nombre_de_pays DESC;


